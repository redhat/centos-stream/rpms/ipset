#!/bin/bash
# SPDX-License-Identifier: LGPL-2.1+
# ~~~
#   runtest.sh of ipset
#   Description: ipset tests.
#
#   Author: Susant Sahani <susant@redhat.com>
#   Copyright (c) 2018 Red Hat, Inc.
# ~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="ipset"

SERVICE_UNITDIR="/var/run/systemd/system"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlAssertRpm kernel-modules-extra-$(uname -r)
        rlRun "systemctl stop firewalld" 0,5

        rlRun "[ -e /sys/class/net/veth-test ] && ip link del veth-test" 0,1

        rlRun "cp iperf3d.service $SERVICE_UNITDIR"
        rlRun "systemctl daemon-reload"

        rlRun "ip link add veth-test type veth peer name veth-peer"
        rlRun "ip link set veth-test addr 02:01:02:03:04:08"
        rlRun "ip link set veth-peer addr 02:01:02:03:04:09"
        rlRun "ip addr add 192.168.225.32/24 dev veth-test"
        rlRun "ip addr add 192.168.225.33/24 dev veth-peer"
        rlRun "ip link set veth-test up"
        rlRun "ip link set veth-peer up"
    rlPhaseEnd

    rlPhaseStartTest "test_ipset_bitmap_ip_netfilter"
        rlRun "ipset create testnetiperf hash:ip"
        rlRun "ipset add testnetiperf 192.168.225.32"
        rlRun "ipset add testnetiperf 192.168.225.33"
        rlRun "ipset test testnetiperf 192.168.225.32"
        rlRun "ipset test testnetiperf 192.168.225.33"
        rlRun "systemctl start iperf3d.service"
        sleep 1
        rlRun "iperf3 -c 192.168.225.32 -p 55555 --connect-timeout 5"
        rlRun "iptables -I INPUT -m set --match-set testnetiperf src -j DROP"
        rlRun "iperf3 -c 192.168.225.32 -p 55555 --connect-timeout 5" 1
        rlRun "systemctl stop iperf3d.service"
        rlRun "iptables --delete INPUT -m set --match-set testnetiperf src -j DROP"
        rlRun "ipset destroy testnetiperf"
    rlPhaseEnd

    rlPhaseStartTest "test_ipset_add_bitmap_ip"
        rlRun "ipset create testnet hash:ip"
        rlRun "ipset add testnet 192.168.11.12"
        rlRun "ipset add testnet 192.168.11.13"
        rlRun "ipset add testnet 192.168.11.14"
        rlRun "ipset add testnet 192.168.11.15"
        rlRun "ipset test testnet 192.168.11.12"
        rlRun "ipset test testnet 192.168.11.13"
        rlRun "ipset test testnet 192.168.11.14"
        rlRun "ipset test testnet 192.168.11.15"
        rlRun "ipset destroy testnet"
    rlPhaseEnd

    rlPhaseStartTest "test_ipset_delete_bitmap_ip"
        rlRun "ipset create testnet hash:ip"
        rlRun "ipset add testnet 192.168.11.12"
        rlRun "ipset add testnet 192.168.11.13"
        rlRun "ipset test testnet 192.168.11.12"
        rlRun "ipset test testnet 192.168.11.13"
        rlRun "ipset del testnet 192.168.11.12"
        rlRun "ipset test testnet 192.168.11.12" 1
        rlRun "ipset destroy testnet"
    rlPhaseEnd

    rlPhaseStartTest "test_ipset_hash_bitmap_mac"
        rlRun "ipset create testmac hash:mac"
        rlRun "ipset add testmac 02:01:02:03:04:09"
        rlRun "ipset test testmac 02:01:02:03:04:09"
        rlRun "ipset del testmac 02:01:02:03:04:09"
        rlRun "ipset test testmac 02:01:02:03:04:09" 1
        rlRun "ipset destroy testmac"
    rlPhaseEnd

    rlPhaseStartTest "test_ipset_hash_bitmap_ipport"
        rlRun "ipset create testipport hash:ip,mac"
        rlRun "ipset add testipport 1.1.1.1,02:01:02:03:04:09"
        rlRun "ipset test testipport 1.1.1.1,02:01:02:03:04:09"
        rlRun "ipset del testipport 1.1.1.1,02:01:02:03:04:09"
        rlRun "ipset test testipport 1.1.1.1,02:01:02:03:04:09" 1
        rlRun "ipset destroy testipport"
    rlPhaseEnd

    rlPhaseStartTest "test_ipset_hash_bitmap_ipport"
        rlRun "ipset create testipport hash:ip,port"
        rlRun "ipset add testipport 192.168.1.1,udp:53"
        rlRun "ipset add testipport 192.168.1.1,5555"
        rlRun "ipset test testipport 192.168.1.1,udp:53"
        rlRun "ipset test testipport 192.168.1.1,5555"
        rlRun "ipset del testipport 192.168.1.1,5555"
        rlRun "ipset test testipport 192.168.1.1,5555" 1
        rlRun "ipset destroy testipport"
    rlPhaseEnd

    rlPhaseStartTest "test_ipset_hash_bitmap_ipportip"
        rlRun "ipset create testipportip hash:ip,port,ip"
        rlRun "ipset add testipportip 192.168.1.1,80,10.0.0.1"
        rlRun "ipset add testipportip 192.168.1.2,80,10.0.0.2"
        rlRun "ipset test testipportip 192.168.1.1,80,10.0.0.1"
        rlRun "ipset test testipportip 192.168.1.1,80,10.0.0.1"
        rlRun "ipset del testipportip 192.168.1.1,80,10.0.0.1"
        rlRun "ipset test testipportip 192.168.1.1,80,10.0.0.1" 1
        rlRun "ipset destroy testipportip"
    rlPhaseEnd

    rlPhaseStartTest "test_ipset_hash_bitmap_netiface"
        rlRun "ipset create testnetiface hash:net,iface"
        rlRun "ipset add testnetiface 192.168.0/24,veth-test"
        rlRun "ipset add testnetiface 192.167.0/24,veth-peer"
        rlRun "ipset test testnetiface 192.168.0/24,veth-test"
        rlRun "ipset test testnetiface 192.167.0/24,veth-peer"
        rlRun "ipset del testnetiface 192.168.0/24,veth-test"
        rlRun "ipset test testnetiface 192.168.0/24,veth-test" 1
        rlRun "ipset destroy testnetiface"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "ip link del veth-test"

        rlRun "rm $SERVICE_UNITDIR/iperf3d.service"
        rlRun "systemctl daemon-reload"

        rlLog "ipset tests done"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

rlGetTestState
